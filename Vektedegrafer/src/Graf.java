import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.*;

public class Graf {


    static class Kant {
        int fra, til, flyt, kapasitet;
        public Kant motsattKant;

        public Kant(int fra, int til, int kapasitet) {
            this.fra = fra;
            this.til = til;
            this.kapasitet = kapasitet;
        }
    }

    static class Node {
        ArrayList<Kant> kanter = new ArrayList<>();
    }

    static class MaksFlyt {
        ArrayList<Svar> svar = new ArrayList<>(); // økning og flytøkende vei for graf
        int maksFlyt;

        public static Node[] nyGraf(URL url) throws IOException {

            BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
            StringTokenizer st = new StringTokenizer(br.readLine());

            int N = Integer.parseInt(st.nextToken());
            int E = Integer.parseInt(st.nextToken());

            Node[] noder = new Node[N];
            for (int i = 0; i < N; i++) {
                noder[i] = new Node();
            }

            for (int i = 0; i < E; i++) {
                st = new StringTokenizer(br.readLine());

                int fra = Integer.parseInt(st.nextToken());
                int til = Integer.parseInt(st.nextToken());

                int kapasitet = Integer.parseInt(st.nextToken());

                if (kapasitet == 0) throw new IllegalArgumentException("Forward edge kapasitet <= 0");
                Kant e1 = new Kant(fra, til, kapasitet);
                Kant e2 = new Kant(til, fra, 0);
                e1.motsattKant = e2;
                e2.motsattKant = e1;
                noder[fra].kanter.add(e1);
                noder[til].kanter.add(e2);
            }
            return noder;
        }

        void edmondsKarp(int start, int slutt, Node[] graf) {
            maksFlyt = 0;
            while (true) {
                Kant[] flowPath = new Kant[graf.length];
                Queue<Node> queue = new ArrayDeque<>();
                queue.add(graf[start]);
                // BFS
                while (!queue.isEmpty()) {

                    Node current = queue.poll(); // fjerner og returnerer node i kø
                    for (Kant e : current.kanter) {
                        if (flowPath[e.til] == null && e.til != start && e.kapasitet > e.flyt) {

                        // lagrer flowpath til bfs slik at man kan jobbe med det senere
                            flowPath[e.til] = e;
                            queue.add(graf[e.til]);
                        }
                    }
                }
                if (flowPath[slutt] == null) {
                    break; // enden ikke nådd
                }
                int pushFlow = Integer.MAX_VALUE;
                for (Kant e = flowPath[slutt]; e != null; e = flowPath[e.fra])
                    pushFlow = Math.min(pushFlow, e.kapasitet - e.flyt);
                // lagrer svar fra BFS søk
                Svar svar = new Svar();
                svar.okning = pushFlow;
                svar.kanter = new LinkedList<>();
                svar.kanter.add(slutt);
                // uppdaterer flow for kanter
                for (Kant e = flowPath[slutt]; e != null; e = flowPath[e.fra]) {
                    e.flyt += pushFlow;
                    e.motsattKant.flyt -= pushFlow;
                    svar.kanter.addFirst(e.fra);
                }
                this.svar.add(svar);
                maksFlyt += pushFlow;
            }
        }

        void printSvar() {
            for (Svar value : svar) {
                System.out.println(value);
            }
        }

        static class Svar {
            int okning;
            LinkedList<Integer> kanter;

            @Override
            public String toString() {
                return "Økning:"+ okning+ " | kanter: "+ kanter;
            }
        }

        public void grafAnalyse(String grafNavn, int start, int slutt, Node[] graf) {
            MaksFlyt mf = new MaksFlyt();
            System.out.println(grafNavn);
            mf.edmondsKarp(start, slutt, graf);
            mf.printSvar();
            System.out.println("Max flyt " + mf.maksFlyt + "\n");
        }

        public static void main(String[] args) throws IOException {
            MaksFlyt mf = new MaksFlyt();
//Graf1
            URL url1 = new URL("http://www.iie.ntnu.no/fag/_alg/v-graf/flytgraf1");
            Node[] graf1 = MaksFlyt.nyGraf(url1);
            String grafNavn1 = " Graf 1";
            mf.grafAnalyse(grafNavn1, 0, 7, graf1);
//graf 2
            URL url2 = new URL("http://www.iie.ntnu.no/fag/_alg/v-graf/flytgraf2");
            Node[] graf2 = MaksFlyt.nyGraf(url2);
            String grafNavn2 = " Graf 2";
            mf.grafAnalyse(grafNavn2, 4, 15, graf2);
//graf 3
            URL url3 = new URL("http://www.iie.ntnu.no/fag/_alg/v-graf/flytgraf3");
            Node[] graf3 = MaksFlyt.nyGraf(url3);
            String grafNavn3 = " Graf 3";
            mf.grafAnalyse(grafNavn3, 3, 15, graf3);
        }
    }
}


